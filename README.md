# Building a Yocto image for SMARC.

This document demonstrates the step by step Yocto porting guide using the Freescale Yocto building enthronement and the SMARC module BSP.

If you wish to build the Yocto project yourself, please follow the below steps:

## 1. Install the repo utility

```bash
$ mkdir ~/bin
$ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
```

## 2. Download FSL Community BSP source

```bash
$ PATH=${PATH}:~/bin
$ mkdir fsl-community-bsp
$ cd fsl-community-bsp
$ repo init -u https://github.com/Freescale/fsl-community-bsp-platform -b krogoth
$ repo sync
```


## 3. Downlaod the bcm-smarc-imx6-bsp repo
```bash
$ git clone https://bitbucket.org/wrrn/bcm-smarc-imx6-bsp
```

## 4. Copy the SMARC BSP into the FSL Community BSP
```bash
$ cp bcm-smarc-imx6-bsp/sources fsl-community-bsp sources
```

## 5. Build
```
$ cd fsl-community-bsp
$ MACHINE=smarc source setup-environment build
$ bitbake (image_name)
```

## 6. Include uboot in sdcard image 

When the build is finished, go to build/tmp/deploy/images/smarc/ directory and see if all images has been built correctly. You should be able to find the sdcard image, rootfs, zImage, and u-boot in this directory for deployment. For some reason the u-boot sometimes don’t get build by default and we are still investigating this issue. If you don’t see u-boot.imx, please build this manually using the following command under your build directory:**
```bash
$ bitbake u-boot-smarc -c fetch
$ bitbake u-boot-smarc -c compile
$ bitbake u-boot-smarc -c deploy
```

Then go back to the build-smarc/tmp/deploy/images/smarc/ directory and see if u-boot.imx is present. If so, please do the below dd command to add u-boot to the sdcard image.
```bash
$ dd if=u-boot.imx of=fsl-image-qt5-smarc.sdcard bs=1k seek=1 conv=notrunc
```


## Other things
### Edit build_smarc/conf/local.conf and with the below code to overwrite **the fsl BSP:**
```
PREFERRED_PROVIDER_u-boot_mx6 = "u-boot-smarc"
PREFERRED_PROVIDER_virtual/kernel_mx6 = "linux-smarc"
```

### You can adjust the rootfs size to utilize more space of your sdcard by adding the below 3 parameter in the file “build_smarc/conf/local.conf”.
```
IMAGE_EXTRA_SPACE = "0"
IMAGE_ROOTFS_EXTRA_SPACE = "0"
IMAGE_ROOTFS_SIZE = "3806250"
```
(this will give you around 3.7GB of rootfs to fit in a 4GB sdcard.)
