require recipes-bsp/u-boot/u-boot.inc

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=c7383a594871c03da76b3707929d2919"
COMPATIBLE_MACHINE = "smarc"

PROVIDES = "u-boot-smarc"

# PV = "uboot-smarc+git${SRCPV}"

SRCREV = "85539539fe6d8c380320ad3315c26ad6b98efa57"
SRCBRANCH = "v2015.04_4.1.15_1.0.0_ga_smarc"
SRC_URI = "git://github.com/bcmadvancedresearch/u-boot_sma-imx6.git;branch=${SRCBRANCH}\
           file://0001-Change-resolution-to-1280x1024.patch"

S = "${WORKDIR}/git"

PACKAGE_ARCH = "${MACHINE_ARCH}"
