# Adapted from linux-imx.inc, copyright (C) 2013, 2014 O.S. Systems Software LTDA
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-kernel/linux/linux-imx.inc
require recipes-kernel/linux/linux-dtb.inc

SUMMARY = "Linux kernel for BCM-SMARC module"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI = "git://github.com/bcmadvancedresearch/kernel_sma-imx6;branch=${SRCBRANCH}\
           file://defconfig\
           file://0001-Add-HDMI-to-DVI-fixes.patch\
           file://0002-Update-the-VGA-drivers.patch\
           file://0003-Add-gcc-6-support.patch"
           
#12/04/2015
SRCBRANCH = "4.1.15-1.0.0"
SRCREV = "6ee12b6e569ec94ae814e3eddf72abb58027617e"
S = "${WORKDIR}/git"
DEPENDS += "lzop-native bc-native"
COMPATIBLE_MACHINE = "smarc"

